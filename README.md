The Mesh Submission directory contains the following:

-	This Read Me document

-	Serval Mesh Notes.doc describing the painstakingly corrected steps needed to configure the Mesh Extender hardware and software. The Serval Github is filled with errors. This document is the bible on how to configure the hardware.

-	RFD Modem.doc another bible. Describes how to query the modem and what the LEDs indicate. 

-	Comms Modes Pictorial Overview.doc describing the 3 different modes the Mesh Extender can operate under.

-	RFD900 Tester.zip Application for testing the raw channel

-	Batphone.zip Cubic Inc. Hackathon App for running on the phone. I would recommend using the Serval App from the Playstore. There were some hiccups with this one but I did not have the time to get down to the App level to figure this out.

-	Class Presentations DIRECTORY   Biweekly status presentations for Capstone class. Usable information in there, especially o’scope graphs describing comms. These plots are only in the presentations.

-	Field Test A, B, C and D DIRECTORIES  Field test data and footage.