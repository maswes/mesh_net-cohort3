﻿namespace RFD900TesterApplication
{
    partial class RFD900SerialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.ResponseTimer = new System.Windows.Forms.Timer(this.components);
            this.RFD900TabControl = new System.Windows.Forms.TabControl();
            this.TestCommsPage = new System.Windows.Forms.TabPage();
            this.LogfileLocationGroupBox = new System.Windows.Forms.GroupBox();
            this.FolderExplorerButton = new System.Windows.Forms.Button();
            this.FileNameTextBox = new System.Windows.Forms.TextBox();
            this.PromptFileBrowseCheckBox = new System.Windows.Forms.CheckBox();
            this.FileDirectoryTextBox = new System.Windows.Forms.TextBox();
            this.BrowseFileNameButton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TripTimeGroupBox = new System.Windows.Forms.GroupBox();
            this.LastTimeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MinTimeTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.MaxTimeTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AvgTimeTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ReceivedLabel = new System.Windows.Forms.Label();
            this.TimeoutLabel = new System.Windows.Forms.Label();
            this.TimeoutTextBox = new System.Windows.Forms.TextBox();
            this.IntervalLabel = new System.Windows.Forms.Label();
            this.IntervalTextBox = new System.Windows.Forms.TextBox();
            this.BoxesSilentCheckBox = new System.Windows.Forms.CheckBox();
            this.ClearBoxesLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.OpenButton = new System.Windows.Forms.Button();
            this.BaudrateComboBox = new System.Windows.Forms.ComboBox();
            this.COMportComboBox = new System.Windows.Forms.ComboBox();
            this.ReceiveInformationLabel = new System.Windows.Forms.Label();
            this.ReceiveRichTextBox = new System.Windows.Forms.RichTextBox();
            this.ReceiveInformationRichTextBox = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.RunDurationTextBox = new System.Windows.Forms.TextBox();
            this.StopOnErrorCheckBox = new System.Windows.Forms.CheckBox();
            this.RespondEnableCheckBox = new System.Windows.Forms.CheckBox();
            this.SendContinuousCheckBox = new System.Windows.Forms.CheckBox();
            this.ErrorTextLabel = new System.Windows.Forms.Label();
            this.ErrorNumberLabel = new System.Windows.Forms.Label();
            this.SequenceNumberLabel = new System.Windows.Forms.Label();
            this.SequenceStartLabel = new System.Windows.Forms.Label();
            this.SequenceStartNumberTextBox = new System.Windows.Forms.TextBox();
            this.SendButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.TxMessageTextBox = new System.Windows.Forms.TextBox();
            this.TransmitInformationLabel = new System.Windows.Forms.Label();
            this.TransmitInformationRichTextBox = new System.Windows.Forms.RichTextBox();
            this.TransmittedLabel = new System.Windows.Forms.Label();
            this.TransmitRichTextBox = new System.Windows.Forms.RichTextBox();
            this.RFD900SettingsPage = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ClearOP_RESEND_Button = new System.Windows.Forms.Button();
            this.SetOP_RESEND_Button = new System.Windows.Forms.Button();
            this.EnterATmodeButton = new System.Windows.Forms.Button();
            this.RFD900CommandComboBox = new System.Windows.Forms.ComboBox();
            this.ClearBoxesRFD900Label = new System.Windows.Forms.Label();
            this.ReceivedRFD900Label = new System.Windows.Forms.Label();
            this.ReceiveInformationRFD900Label = new System.Windows.Forms.Label();
            this.ReceiveRFD900RichTextBox = new System.Windows.Forms.RichTextBox();
            this.ReceiveInformationRFD900RichTextBox = new System.Windows.Forms.RichTextBox();
            this.TransmitInformationRFD900Label = new System.Windows.Forms.Label();
            this.TransmitInformationRFD900RichTextBox = new System.Windows.Forms.RichTextBox();
            this.TransmittedRFD900Label = new System.Windows.Forms.Label();
            this.TransmitRFD900RichTextBox = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.RFD900InfoButton = new System.Windows.Forms.Button();
            this.CRcheckBox = new System.Windows.Forms.CheckBox();
            this.SendRawButton = new System.Windows.Forms.Button();
            this.EnableFileLoggingCheckBox = new System.Windows.Forms.CheckBox();
            this.RFD900TabControl.SuspendLayout();
            this.TestCommsPage.SuspendLayout();
            this.LogfileLocationGroupBox.SuspendLayout();
            this.TripTimeGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.RFD900SettingsPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 57600;
            this.serialPort1.PortName = "COM8";
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // ResponseTimer
            // 
            this.ResponseTimer.Interval = 5000;
            this.ResponseTimer.Tick += new System.EventHandler(this.ResponseTimer_Tick);
            // 
            // RFD900TabControl
            // 
            this.RFD900TabControl.Controls.Add(this.TestCommsPage);
            this.RFD900TabControl.Controls.Add(this.RFD900SettingsPage);
            this.RFD900TabControl.Location = new System.Drawing.Point(1, 2);
            this.RFD900TabControl.Name = "RFD900TabControl";
            this.RFD900TabControl.SelectedIndex = 0;
            this.RFD900TabControl.Size = new System.Drawing.Size(1132, 715);
            this.RFD900TabControl.TabIndex = 124;
            // 
            // TestCommsPage
            // 
            this.TestCommsPage.Controls.Add(this.LogfileLocationGroupBox);
            this.TestCommsPage.Controls.Add(this.TripTimeGroupBox);
            this.TestCommsPage.Controls.Add(this.label8);
            this.TestCommsPage.Controls.Add(this.ReceivedLabel);
            this.TestCommsPage.Controls.Add(this.TimeoutLabel);
            this.TestCommsPage.Controls.Add(this.TimeoutTextBox);
            this.TestCommsPage.Controls.Add(this.IntervalLabel);
            this.TestCommsPage.Controls.Add(this.IntervalTextBox);
            this.TestCommsPage.Controls.Add(this.BoxesSilentCheckBox);
            this.TestCommsPage.Controls.Add(this.ClearBoxesLabel);
            this.TestCommsPage.Controls.Add(this.groupBox2);
            this.TestCommsPage.Controls.Add(this.ReceiveInformationLabel);
            this.TestCommsPage.Controls.Add(this.ReceiveRichTextBox);
            this.TestCommsPage.Controls.Add(this.ReceiveInformationRichTextBox);
            this.TestCommsPage.Controls.Add(this.label5);
            this.TestCommsPage.Controls.Add(this.RunDurationTextBox);
            this.TestCommsPage.Controls.Add(this.StopOnErrorCheckBox);
            this.TestCommsPage.Controls.Add(this.RespondEnableCheckBox);
            this.TestCommsPage.Controls.Add(this.SendContinuousCheckBox);
            this.TestCommsPage.Controls.Add(this.ErrorTextLabel);
            this.TestCommsPage.Controls.Add(this.ErrorNumberLabel);
            this.TestCommsPage.Controls.Add(this.SequenceNumberLabel);
            this.TestCommsPage.Controls.Add(this.SequenceStartLabel);
            this.TestCommsPage.Controls.Add(this.SequenceStartNumberTextBox);
            this.TestCommsPage.Controls.Add(this.SendButton);
            this.TestCommsPage.Controls.Add(this.label6);
            this.TestCommsPage.Controls.Add(this.TxMessageTextBox);
            this.TestCommsPage.Controls.Add(this.TransmitInformationLabel);
            this.TestCommsPage.Controls.Add(this.TransmitInformationRichTextBox);
            this.TestCommsPage.Controls.Add(this.TransmittedLabel);
            this.TestCommsPage.Controls.Add(this.TransmitRichTextBox);
            this.TestCommsPage.Location = new System.Drawing.Point(4, 22);
            this.TestCommsPage.Name = "TestCommsPage";
            this.TestCommsPage.Padding = new System.Windows.Forms.Padding(3);
            this.TestCommsPage.Size = new System.Drawing.Size(1124, 689);
            this.TestCommsPage.TabIndex = 0;
            this.TestCommsPage.Text = "Test Comms";
            this.TestCommsPage.UseVisualStyleBackColor = true;
            // 
            // LogfileLocationGroupBox
            // 
            this.LogfileLocationGroupBox.Controls.Add(this.EnableFileLoggingCheckBox);
            this.LogfileLocationGroupBox.Controls.Add(this.FolderExplorerButton);
            this.LogfileLocationGroupBox.Controls.Add(this.FileNameTextBox);
            this.LogfileLocationGroupBox.Controls.Add(this.PromptFileBrowseCheckBox);
            this.LogfileLocationGroupBox.Controls.Add(this.FileDirectoryTextBox);
            this.LogfileLocationGroupBox.Controls.Add(this.BrowseFileNameButton);
            this.LogfileLocationGroupBox.Controls.Add(this.label10);
            this.LogfileLocationGroupBox.Controls.Add(this.label9);
            this.LogfileLocationGroupBox.Location = new System.Drawing.Point(547, 13);
            this.LogfileLocationGroupBox.Name = "LogfileLocationGroupBox";
            this.LogfileLocationGroupBox.Size = new System.Drawing.Size(390, 157);
            this.LogfileLocationGroupBox.TabIndex = 169;
            this.LogfileLocationGroupBox.TabStop = false;
            this.LogfileLocationGroupBox.Text = "Log file location";
            // 
            // FolderExplorerButton
            // 
            this.FolderExplorerButton.Location = new System.Drawing.Point(303, 75);
            this.FolderExplorerButton.Name = "FolderExplorerButton";
            this.FolderExplorerButton.Size = new System.Drawing.Size(80, 23);
            this.FolderExplorerButton.TabIndex = 170;
            this.FolderExplorerButton.Text = "Explore Logs";
            this.FolderExplorerButton.UseVisualStyleBackColor = true;
            this.FolderExplorerButton.Click += new System.EventHandler(this.FolderExplorerButton_Click);
            // 
            // FileNameTextBox
            // 
            this.FileNameTextBox.Location = new System.Drawing.Point(66, 21);
            this.FileNameTextBox.Name = "FileNameTextBox";
            this.FileNameTextBox.Size = new System.Drawing.Size(318, 20);
            this.FileNameTextBox.TabIndex = 164;
            // 
            // PromptFileBrowseCheckBox
            // 
            this.PromptFileBrowseCheckBox.AutoSize = true;
            this.PromptFileBrowseCheckBox.Location = new System.Drawing.Point(66, 132);
            this.PromptFileBrowseCheckBox.Name = "PromptFileBrowseCheckBox";
            this.PromptFileBrowseCheckBox.Size = new System.Drawing.Size(95, 17);
            this.PromptFileBrowseCheckBox.TabIndex = 168;
            this.PromptFileBrowseCheckBox.Text = "Always Prompt";
            this.PromptFileBrowseCheckBox.UseVisualStyleBackColor = true;
            // 
            // FileDirectoryTextBox
            // 
            this.FileDirectoryTextBox.Location = new System.Drawing.Point(66, 50);
            this.FileDirectoryTextBox.Name = "FileDirectoryTextBox";
            this.FileDirectoryTextBox.Size = new System.Drawing.Size(317, 20);
            this.FileDirectoryTextBox.TabIndex = 165;
            // 
            // BrowseFileNameButton
            // 
            this.BrowseFileNameButton.Location = new System.Drawing.Point(66, 76);
            this.BrowseFileNameButton.Name = "BrowseFileNameButton";
            this.BrowseFileNameButton.Size = new System.Drawing.Size(75, 23);
            this.BrowseFileNameButton.TabIndex = 163;
            this.BrowseFileNameButton.Text = "Browse File";
            this.BrowseFileNameButton.UseVisualStyleBackColor = true;
            this.BrowseFileNameButton.Click += new System.EventHandler(this.BrowseFileNameButton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 167;
            this.label10.Text = "Directory";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 166;
            this.label9.Text = "File Name";
            // 
            // TripTimeGroupBox
            // 
            this.TripTimeGroupBox.Controls.Add(this.LastTimeTextBox);
            this.TripTimeGroupBox.Controls.Add(this.label1);
            this.TripTimeGroupBox.Controls.Add(this.MinTimeTextBox);
            this.TripTimeGroupBox.Controls.Add(this.label2);
            this.TripTimeGroupBox.Controls.Add(this.MaxTimeTextBox);
            this.TripTimeGroupBox.Controls.Add(this.label3);
            this.TripTimeGroupBox.Controls.Add(this.AvgTimeTextBox);
            this.TripTimeGroupBox.Controls.Add(this.label4);
            this.TripTimeGroupBox.Location = new System.Drawing.Point(13, 96);
            this.TripTimeGroupBox.Name = "TripTimeGroupBox";
            this.TripTimeGroupBox.Size = new System.Drawing.Size(318, 68);
            this.TripTimeGroupBox.TabIndex = 161;
            this.TripTimeGroupBox.TabStop = false;
            this.TripTimeGroupBox.Text = "Round Trip Stats";
            // 
            // LastTimeTextBox
            // 
            this.LastTimeTextBox.Location = new System.Drawing.Point(6, 31);
            this.LastTimeTextBox.Name = "LastTimeTextBox";
            this.LastTimeTextBox.Size = new System.Drawing.Size(72, 20);
            this.LastTimeTextBox.TabIndex = 139;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 140;
            this.label1.Text = "Last ms";
            // 
            // MinTimeTextBox
            // 
            this.MinTimeTextBox.Location = new System.Drawing.Point(84, 31);
            this.MinTimeTextBox.Name = "MinTimeTextBox";
            this.MinTimeTextBox.Size = new System.Drawing.Size(72, 20);
            this.MinTimeTextBox.TabIndex = 141;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 142;
            this.label2.Text = "Min ms";
            // 
            // MaxTimeTextBox
            // 
            this.MaxTimeTextBox.Location = new System.Drawing.Point(162, 31);
            this.MaxTimeTextBox.Name = "MaxTimeTextBox";
            this.MaxTimeTextBox.Size = new System.Drawing.Size(72, 20);
            this.MaxTimeTextBox.TabIndex = 143;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(162, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 144;
            this.label3.Text = "Max ms";
            // 
            // AvgTimeTextBox
            // 
            this.AvgTimeTextBox.Location = new System.Drawing.Point(240, 31);
            this.AvgTimeTextBox.Name = "AvgTimeTextBox";
            this.AvgTimeTextBox.Size = new System.Drawing.Size(72, 20);
            this.AvgTimeTextBox.TabIndex = 145;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(240, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 146;
            this.label4.Text = "Avg ms";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(151, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 160;
            this.label8.Text = "Current Sequence #";
            // 
            // ReceivedLabel
            // 
            this.ReceivedLabel.AutoSize = true;
            this.ReceivedLabel.Location = new System.Drawing.Point(547, 373);
            this.ReceivedLabel.Name = "ReceivedLabel";
            this.ReceivedLabel.Size = new System.Drawing.Size(53, 13);
            this.ReceivedLabel.TabIndex = 159;
            this.ReceivedLabel.Text = "Received";
            this.ReceivedLabel.Click += new System.EventHandler(this.ReceivedLabel_Click);
            // 
            // TimeoutLabel
            // 
            this.TimeoutLabel.AutoSize = true;
            this.TimeoutLabel.Location = new System.Drawing.Point(334, 13);
            this.TimeoutLabel.Name = "TimeoutLabel";
            this.TimeoutLabel.Size = new System.Drawing.Size(72, 13);
            this.TimeoutLabel.TabIndex = 158;
            this.TimeoutLabel.Text = "Timeout in ms";
            // 
            // TimeoutTextBox
            // 
            this.TimeoutTextBox.Location = new System.Drawing.Point(337, 34);
            this.TimeoutTextBox.Name = "TimeoutTextBox";
            this.TimeoutTextBox.Size = new System.Drawing.Size(75, 20);
            this.TimeoutTextBox.TabIndex = 157;
            this.TimeoutTextBox.Text = "2000";
            // 
            // IntervalLabel
            // 
            this.IntervalLabel.AutoSize = true;
            this.IntervalLabel.Location = new System.Drawing.Point(442, 15);
            this.IntervalLabel.Name = "IntervalLabel";
            this.IntervalLabel.Size = new System.Drawing.Size(69, 13);
            this.IntervalLabel.TabIndex = 156;
            this.IntervalLabel.Text = "Interval in ms";
            // 
            // IntervalTextBox
            // 
            this.IntervalTextBox.Location = new System.Drawing.Point(445, 34);
            this.IntervalTextBox.Name = "IntervalTextBox";
            this.IntervalTextBox.Size = new System.Drawing.Size(75, 20);
            this.IntervalTextBox.TabIndex = 155;
            this.IntervalTextBox.Text = "500";
            // 
            // BoxesSilentCheckBox
            // 
            this.BoxesSilentCheckBox.AutoSize = true;
            this.BoxesSilentCheckBox.Location = new System.Drawing.Point(954, 12);
            this.BoxesSilentCheckBox.Name = "BoxesSilentCheckBox";
            this.BoxesSilentCheckBox.Size = new System.Drawing.Size(52, 17);
            this.BoxesSilentCheckBox.TabIndex = 154;
            this.BoxesSilentCheckBox.Text = "Silent";
            this.BoxesSilentCheckBox.UseVisualStyleBackColor = true;
            this.BoxesSilentCheckBox.CheckedChanged += new System.EventHandler(this.BoxesSilentCheckBox_CheckedChanged);
            // 
            // ClearBoxesLabel
            // 
            this.ClearBoxesLabel.AutoSize = true;
            this.ClearBoxesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearBoxesLabel.Location = new System.Drawing.Point(1012, 13);
            this.ClearBoxesLabel.Name = "ClearBoxesLabel";
            this.ClearBoxesLabel.Size = new System.Drawing.Size(103, 13);
            this.ClearBoxesLabel.TabIndex = 152;
            this.ClearBoxesLabel.Text = "Clear Text Boxes";
            this.ClearBoxesLabel.Click += new System.EventHandler(this.ClearBoxesLabel_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CloseButton);
            this.groupBox2.Controls.Add(this.OpenButton);
            this.groupBox2.Controls.Add(this.BaudrateComboBox);
            this.groupBox2.Controls.Add(this.COMportComboBox);
            this.groupBox2.Location = new System.Drawing.Point(954, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(161, 135);
            this.groupBox2.TabIndex = 153;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "USB Serial Comms Port";
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(14, 105);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 69;
            this.CloseButton.Text = "Close Port";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // OpenButton
            // 
            this.OpenButton.Location = new System.Drawing.Point(14, 76);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(75, 23);
            this.OpenButton.TabIndex = 68;
            this.OpenButton.Text = "Open Port";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // BaudrateComboBox
            // 
            this.BaudrateComboBox.FormattingEnabled = true;
            this.BaudrateComboBox.Location = new System.Drawing.Point(14, 19);
            this.BaudrateComboBox.Name = "BaudrateComboBox";
            this.BaudrateComboBox.Size = new System.Drawing.Size(121, 21);
            this.BaudrateComboBox.TabIndex = 72;
            // 
            // COMportComboBox
            // 
            this.COMportComboBox.FormattingEnabled = true;
            this.COMportComboBox.Location = new System.Drawing.Point(14, 46);
            this.COMportComboBox.Name = "COMportComboBox";
            this.COMportComboBox.Size = new System.Drawing.Size(121, 21);
            this.COMportComboBox.TabIndex = 73;
            // 
            // ReceiveInformationLabel
            // 
            this.ReceiveInformationLabel.AutoSize = true;
            this.ReceiveInformationLabel.Location = new System.Drawing.Point(544, 173);
            this.ReceiveInformationLabel.Name = "ReceiveInformationLabel";
            this.ReceiveInformationLabel.Size = new System.Drawing.Size(102, 13);
            this.ReceiveInformationLabel.TabIndex = 151;
            this.ReceiveInformationLabel.Text = "Receive Information";
            this.ReceiveInformationLabel.Click += new System.EventHandler(this.ReceiveInformationLabel_Click);
            // 
            // ReceiveRichTextBox
            // 
            this.ReceiveRichTextBox.Location = new System.Drawing.Point(547, 392);
            this.ReceiveRichTextBox.Name = "ReceiveRichTextBox";
            this.ReceiveRichTextBox.Size = new System.Drawing.Size(568, 294);
            this.ReceiveRichTextBox.TabIndex = 150;
            this.ReceiveRichTextBox.Text = "";
            this.ReceiveRichTextBox.TextChanged += new System.EventHandler(this.ReceiveRichTextBox_TextChanged);
            // 
            // ReceiveInformationRichTextBox
            // 
            this.ReceiveInformationRichTextBox.Location = new System.Drawing.Point(547, 189);
            this.ReceiveInformationRichTextBox.Name = "ReceiveInformationRichTextBox";
            this.ReceiveInformationRichTextBox.Size = new System.Drawing.Size(568, 172);
            this.ReceiveInformationRichTextBox.TabIndex = 149;
            this.ReceiveInformationRichTextBox.Text = "";
            this.ReceiveInformationRichTextBox.TextChanged += new System.EventHandler(this.ReceiveInformationRichTextBox_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(337, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 148;
            this.label5.Text = "Run time DHMS";
            // 
            // RunDurationTextBox
            // 
            this.RunDurationTextBox.Location = new System.Drawing.Point(337, 82);
            this.RunDurationTextBox.Name = "RunDurationTextBox";
            this.RunDurationTextBox.Size = new System.Drawing.Size(84, 20);
            this.RunDurationTextBox.TabIndex = 147;
            // 
            // StopOnErrorCheckBox
            // 
            this.StopOnErrorCheckBox.AutoSize = true;
            this.StopOnErrorCheckBox.Location = new System.Drawing.Point(154, 13);
            this.StopOnErrorCheckBox.Name = "StopOnErrorCheckBox";
            this.StopOnErrorCheckBox.Size = new System.Drawing.Size(88, 17);
            this.StopOnErrorCheckBox.TabIndex = 138;
            this.StopOnErrorCheckBox.Text = "Stop on Error";
            this.StopOnErrorCheckBox.UseVisualStyleBackColor = true;
            this.StopOnErrorCheckBox.CheckedChanged += new System.EventHandler(this.StopOnErrorCheckBox_CheckedChanged);
            // 
            // RespondEnableCheckBox
            // 
            this.RespondEnableCheckBox.AutoSize = true;
            this.RespondEnableCheckBox.Location = new System.Drawing.Point(451, 211);
            this.RespondEnableCheckBox.Name = "RespondEnableCheckBox";
            this.RespondEnableCheckBox.Size = new System.Drawing.Size(69, 17);
            this.RespondEnableCheckBox.TabIndex = 137;
            this.RespondEnableCheckBox.Text = "Respond";
            this.RespondEnableCheckBox.UseVisualStyleBackColor = true;
            this.RespondEnableCheckBox.CheckedChanged += new System.EventHandler(this.RespondEnableCheckBox_CheckedChanged);
            // 
            // SendContinuousCheckBox
            // 
            this.SendContinuousCheckBox.AutoSize = true;
            this.SendContinuousCheckBox.Location = new System.Drawing.Point(451, 187);
            this.SendContinuousCheckBox.Name = "SendContinuousCheckBox";
            this.SendContinuousCheckBox.Size = new System.Drawing.Size(79, 17);
            this.SendContinuousCheckBox.TabIndex = 136;
            this.SendContinuousCheckBox.Text = "Continuous";
            this.SendContinuousCheckBox.UseVisualStyleBackColor = true;
            this.SendContinuousCheckBox.CheckedChanged += new System.EventHandler(this.SendContinuousCheckBox_CheckedChanged);
            // 
            // ErrorTextLabel
            // 
            this.ErrorTextLabel.AutoSize = true;
            this.ErrorTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorTextLabel.Location = new System.Drawing.Point(75, 17);
            this.ErrorTextLabel.Name = "ErrorTextLabel";
            this.ErrorTextLabel.Size = new System.Drawing.Size(40, 13);
            this.ErrorTextLabel.TabIndex = 135;
            this.ErrorTextLabel.Text = "Errors";
            // 
            // ErrorNumberLabel
            // 
            this.ErrorNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorNumberLabel.Location = new System.Drawing.Point(35, 17);
            this.ErrorNumberLabel.Name = "ErrorNumberLabel";
            this.ErrorNumberLabel.Size = new System.Drawing.Size(34, 13);
            this.ErrorNumberLabel.TabIndex = 134;
            this.ErrorNumberLabel.Text = "Errs";
            this.ErrorNumberLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ErrorNumberLabel.DoubleClick += new System.EventHandler(this.ErrorNumberLabel_DoubleClick);
            // 
            // SequenceNumberLabel
            // 
            this.SequenceNumberLabel.AutoSize = true;
            this.SequenceNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SequenceNumberLabel.Location = new System.Drawing.Point(151, 65);
            this.SequenceNumberLabel.Name = "SequenceNumberLabel";
            this.SequenceNumberLabel.Size = new System.Drawing.Size(107, 13);
            this.SequenceNumberLabel.TabIndex = 133;
            this.SequenceNumberLabel.Text = "SequenceNumber";
            // 
            // SequenceStartLabel
            // 
            this.SequenceStartLabel.AutoSize = true;
            this.SequenceStartLabel.Location = new System.Drawing.Point(16, 46);
            this.SequenceStartLabel.Name = "SequenceStartLabel";
            this.SequenceStartLabel.Size = new System.Drawing.Size(89, 13);
            this.SequenceStartLabel.TabIndex = 132;
            this.SequenceStartLabel.Text = "Sequence # start";
            // 
            // SequenceStartNumberTextBox
            // 
            this.SequenceStartNumberTextBox.Location = new System.Drawing.Point(15, 62);
            this.SequenceStartNumberTextBox.Name = "SequenceStartNumberTextBox";
            this.SequenceStartNumberTextBox.Size = new System.Drawing.Size(100, 20);
            this.SequenceStartNumberTextBox.TabIndex = 131;
            this.SequenceStartNumberTextBox.Text = "000000";
            // 
            // SendButton
            // 
            this.SendButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SendButton.Location = new System.Drawing.Point(337, 187);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(75, 23);
            this.SendButton.TabIndex = 130;
            this.SendButton.Text = "Send";
            this.SendButton.UseVisualStyleBackColor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 129;
            this.label6.Text = "String to Send";
            // 
            // TxMessageTextBox
            // 
            this.TxMessageTextBox.Location = new System.Drawing.Point(12, 187);
            this.TxMessageTextBox.Multiline = true;
            this.TxMessageTextBox.Name = "TxMessageTextBox";
            this.TxMessageTextBox.Size = new System.Drawing.Size(310, 20);
            this.TxMessageTextBox.TabIndex = 128;
            this.TxMessageTextBox.Text = "Here is my Ping";
            // 
            // TransmitInformationLabel
            // 
            this.TransmitInformationLabel.AutoSize = true;
            this.TransmitInformationLabel.Location = new System.Drawing.Point(16, 222);
            this.TransmitInformationLabel.Name = "TransmitInformationLabel";
            this.TransmitInformationLabel.Size = new System.Drawing.Size(102, 13);
            this.TransmitInformationLabel.TabIndex = 127;
            this.TransmitInformationLabel.Text = "Transmit Information";
            this.TransmitInformationLabel.Click += new System.EventHandler(this.TransmitInformationLabel_Click);
            // 
            // TransmitInformationRichTextBox
            // 
            this.TransmitInformationRichTextBox.Location = new System.Drawing.Point(13, 238);
            this.TransmitInformationRichTextBox.Name = "TransmitInformationRichTextBox";
            this.TransmitInformationRichTextBox.Size = new System.Drawing.Size(528, 123);
            this.TransmitInformationRichTextBox.TabIndex = 126;
            this.TransmitInformationRichTextBox.Text = "";
            this.TransmitInformationRichTextBox.TextChanged += new System.EventHandler(this.TransmitInformationRichTextBox_TextChanged);
            // 
            // TransmittedLabel
            // 
            this.TransmittedLabel.AutoSize = true;
            this.TransmittedLabel.Location = new System.Drawing.Point(13, 373);
            this.TransmittedLabel.Name = "TransmittedLabel";
            this.TransmittedLabel.Size = new System.Drawing.Size(62, 13);
            this.TransmittedLabel.TabIndex = 125;
            this.TransmittedLabel.Text = "Transmitted";
            this.TransmittedLabel.Click += new System.EventHandler(this.TransmittedLabel_Click);
            // 
            // TransmitRichTextBox
            // 
            this.TransmitRichTextBox.Location = new System.Drawing.Point(13, 392);
            this.TransmitRichTextBox.Name = "TransmitRichTextBox";
            this.TransmitRichTextBox.Size = new System.Drawing.Size(528, 294);
            this.TransmitRichTextBox.TabIndex = 124;
            this.TransmitRichTextBox.Text = "";
            this.TransmitRichTextBox.TextChanged += new System.EventHandler(this.TransmitRichTextBox_TextChanged);
            // 
            // RFD900SettingsPage
            // 
            this.RFD900SettingsPage.Controls.Add(this.label12);
            this.RFD900SettingsPage.Controls.Add(this.label11);
            this.RFD900SettingsPage.Controls.Add(this.ClearOP_RESEND_Button);
            this.RFD900SettingsPage.Controls.Add(this.SetOP_RESEND_Button);
            this.RFD900SettingsPage.Controls.Add(this.EnterATmodeButton);
            this.RFD900SettingsPage.Controls.Add(this.RFD900CommandComboBox);
            this.RFD900SettingsPage.Controls.Add(this.ClearBoxesRFD900Label);
            this.RFD900SettingsPage.Controls.Add(this.ReceivedRFD900Label);
            this.RFD900SettingsPage.Controls.Add(this.ReceiveInformationRFD900Label);
            this.RFD900SettingsPage.Controls.Add(this.ReceiveRFD900RichTextBox);
            this.RFD900SettingsPage.Controls.Add(this.ReceiveInformationRFD900RichTextBox);
            this.RFD900SettingsPage.Controls.Add(this.TransmitInformationRFD900Label);
            this.RFD900SettingsPage.Controls.Add(this.TransmitInformationRFD900RichTextBox);
            this.RFD900SettingsPage.Controls.Add(this.TransmittedRFD900Label);
            this.RFD900SettingsPage.Controls.Add(this.TransmitRFD900RichTextBox);
            this.RFD900SettingsPage.Controls.Add(this.label7);
            this.RFD900SettingsPage.Controls.Add(this.RFD900InfoButton);
            this.RFD900SettingsPage.Controls.Add(this.CRcheckBox);
            this.RFD900SettingsPage.Controls.Add(this.SendRawButton);
            this.RFD900SettingsPage.Location = new System.Drawing.Point(4, 22);
            this.RFD900SettingsPage.Name = "RFD900SettingsPage";
            this.RFD900SettingsPage.Padding = new System.Windows.Forms.Padding(3);
            this.RFD900SettingsPage.Size = new System.Drawing.Size(1124, 689);
            this.RFD900SettingsPage.TabIndex = 1;
            this.RFD900SettingsPage.Text = "RFD900 Settings";
            this.RFD900SettingsPage.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(131, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(221, 13);
            this.label12.TabIndex = 174;
            this.label12.Text = "OP_RESEND=0, works with this test program";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(131, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(299, 13);
            this.label11.TabIndex = 173;
            this.label11.Text = "Default for Mesh Extender (causes comms errors with this test)";
            // 
            // ClearOP_RESEND_Button
            // 
            this.ClearOP_RESEND_Button.Location = new System.Drawing.Point(30, 42);
            this.ClearOP_RESEND_Button.Name = "ClearOP_RESEND_Button";
            this.ClearOP_RESEND_Button.Size = new System.Drawing.Size(75, 23);
            this.ClearOP_RESEND_Button.TabIndex = 172;
            this.ClearOP_RESEND_Button.Text = "Clear ATS7";
            this.ClearOP_RESEND_Button.UseVisualStyleBackColor = true;
            this.ClearOP_RESEND_Button.Click += new System.EventHandler(this.ClearOP_RESEND_Button_Click);
            // 
            // SetOP_RESEND_Button
            // 
            this.SetOP_RESEND_Button.Location = new System.Drawing.Point(29, 11);
            this.SetOP_RESEND_Button.Name = "SetOP_RESEND_Button";
            this.SetOP_RESEND_Button.Size = new System.Drawing.Size(75, 23);
            this.SetOP_RESEND_Button.TabIndex = 171;
            this.SetOP_RESEND_Button.Text = "Set ATS7";
            this.SetOP_RESEND_Button.UseVisualStyleBackColor = true;
            this.SetOP_RESEND_Button.Click += new System.EventHandler(this.SetOP_RESEND_Button_Click);
            // 
            // EnterATmodeButton
            // 
            this.EnterATmodeButton.Location = new System.Drawing.Point(302, 70);
            this.EnterATmodeButton.Name = "EnterATmodeButton";
            this.EnterATmodeButton.Size = new System.Drawing.Size(75, 23);
            this.EnterATmodeButton.TabIndex = 170;
            this.EnterATmodeButton.Text = "AT Mode";
            this.EnterATmodeButton.UseVisualStyleBackColor = true;
            this.EnterATmodeButton.Click += new System.EventHandler(this.EnterATmodeButton_Click);
            // 
            // RFD900CommandComboBox
            // 
            this.RFD900CommandComboBox.FormattingEnabled = true;
            this.RFD900CommandComboBox.Items.AddRange(new object[] {
            "ATI",
            "ATI1",
            "ATI2",
            "ATI3",
            "ATI4",
            "ATI5",
            "ATO"});
            this.RFD900CommandComboBox.Location = new System.Drawing.Point(29, 109);
            this.RFD900CommandComboBox.Name = "RFD900CommandComboBox";
            this.RFD900CommandComboBox.Size = new System.Drawing.Size(187, 21);
            this.RFD900CommandComboBox.TabIndex = 169;
            this.RFD900CommandComboBox.Text = "ATO";
            // 
            // ClearBoxesRFD900Label
            // 
            this.ClearBoxesRFD900Label.AutoSize = true;
            this.ClearBoxesRFD900Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearBoxesRFD900Label.Location = new System.Drawing.Point(1004, 11);
            this.ClearBoxesRFD900Label.Name = "ClearBoxesRFD900Label";
            this.ClearBoxesRFD900Label.Size = new System.Drawing.Size(103, 13);
            this.ClearBoxesRFD900Label.TabIndex = 168;
            this.ClearBoxesRFD900Label.Text = "Clear Text Boxes";
            this.ClearBoxesRFD900Label.Click += new System.EventHandler(this.ClearBoxesRFD900Label_Click);
            // 
            // ReceivedRFD900Label
            // 
            this.ReceivedRFD900Label.AutoSize = true;
            this.ReceivedRFD900Label.Location = new System.Drawing.Point(550, 222);
            this.ReceivedRFD900Label.Name = "ReceivedRFD900Label";
            this.ReceivedRFD900Label.Size = new System.Drawing.Size(53, 13);
            this.ReceivedRFD900Label.TabIndex = 167;
            this.ReceivedRFD900Label.Text = "Received";
            this.ReceivedRFD900Label.Click += new System.EventHandler(this.ReceivedRFD900Label_Click);
            // 
            // ReceiveInformationRFD900Label
            // 
            this.ReceiveInformationRFD900Label.AutoSize = true;
            this.ReceiveInformationRFD900Label.Location = new System.Drawing.Point(547, 26);
            this.ReceiveInformationRFD900Label.Name = "ReceiveInformationRFD900Label";
            this.ReceiveInformationRFD900Label.Size = new System.Drawing.Size(102, 13);
            this.ReceiveInformationRFD900Label.TabIndex = 166;
            this.ReceiveInformationRFD900Label.Text = "Receive Information";
            this.ReceiveInformationRFD900Label.Click += new System.EventHandler(this.ReceiveInformationRFD900Label_Click);
            // 
            // ReceiveRFD900RichTextBox
            // 
            this.ReceiveRFD900RichTextBox.Location = new System.Drawing.Point(550, 238);
            this.ReceiveRFD900RichTextBox.Name = "ReceiveRFD900RichTextBox";
            this.ReceiveRFD900RichTextBox.Size = new System.Drawing.Size(568, 448);
            this.ReceiveRFD900RichTextBox.TabIndex = 165;
            this.ReceiveRFD900RichTextBox.Text = "";
            this.ReceiveRFD900RichTextBox.TextChanged += new System.EventHandler(this.ReceiveRFD900RichTextBox_TextChanged);
            // 
            // ReceiveInformationRFD900RichTextBox
            // 
            this.ReceiveInformationRFD900RichTextBox.Location = new System.Drawing.Point(550, 42);
            this.ReceiveInformationRFD900RichTextBox.Name = "ReceiveInformationRFD900RichTextBox";
            this.ReceiveInformationRFD900RichTextBox.Size = new System.Drawing.Size(568, 172);
            this.ReceiveInformationRFD900RichTextBox.TabIndex = 164;
            this.ReceiveInformationRFD900RichTextBox.Text = "";
            this.ReceiveInformationRFD900RichTextBox.TextChanged += new System.EventHandler(this.ReceiveInformationRFD900RichTextBox_TextChanged);
            // 
            // TransmitInformationRFD900Label
            // 
            this.TransmitInformationRFD900Label.AutoSize = true;
            this.TransmitInformationRFD900Label.Location = new System.Drawing.Point(19, 222);
            this.TransmitInformationRFD900Label.Name = "TransmitInformationRFD900Label";
            this.TransmitInformationRFD900Label.Size = new System.Drawing.Size(102, 13);
            this.TransmitInformationRFD900Label.TabIndex = 163;
            this.TransmitInformationRFD900Label.Text = "Transmit Information";
            this.TransmitInformationRFD900Label.Click += new System.EventHandler(this.TransmitInformationRFD900Label_Click);
            // 
            // TransmitInformationRFD900RichTextBox
            // 
            this.TransmitInformationRFD900RichTextBox.Location = new System.Drawing.Point(16, 238);
            this.TransmitInformationRFD900RichTextBox.Name = "TransmitInformationRFD900RichTextBox";
            this.TransmitInformationRFD900RichTextBox.Size = new System.Drawing.Size(528, 123);
            this.TransmitInformationRFD900RichTextBox.TabIndex = 162;
            this.TransmitInformationRFD900RichTextBox.Text = "";
            this.TransmitInformationRFD900RichTextBox.TextChanged += new System.EventHandler(this.TransmitInformationRFD900RichTextBox_TextChanged);
            // 
            // TransmittedRFD900Label
            // 
            this.TransmittedRFD900Label.AutoSize = true;
            this.TransmittedRFD900Label.Location = new System.Drawing.Point(16, 373);
            this.TransmittedRFD900Label.Name = "TransmittedRFD900Label";
            this.TransmittedRFD900Label.Size = new System.Drawing.Size(62, 13);
            this.TransmittedRFD900Label.TabIndex = 161;
            this.TransmittedRFD900Label.Text = "Transmitted";
            this.TransmittedRFD900Label.Click += new System.EventHandler(this.TransmittedRFD900Label_Click);
            // 
            // TransmitRFD900RichTextBox
            // 
            this.TransmitRFD900RichTextBox.Location = new System.Drawing.Point(16, 392);
            this.TransmitRFD900RichTextBox.Name = "TransmitRFD900RichTextBox";
            this.TransmitRFD900RichTextBox.Size = new System.Drawing.Size(528, 294);
            this.TransmitRFD900RichTextBox.TabIndex = 160;
            this.TransmitRFD900RichTextBox.Text = "";
            this.TransmitRFD900RichTextBox.TextChanged += new System.EventHandler(this.TransmitRFD900RichTextBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 101;
            this.label7.Text = "Command to Send";
            // 
            // RFD900InfoButton
            // 
            this.RFD900InfoButton.Location = new System.Drawing.Point(302, 146);
            this.RFD900InfoButton.Name = "RFD900InfoButton";
            this.RFD900InfoButton.Size = new System.Drawing.Size(75, 23);
            this.RFD900InfoButton.TabIndex = 99;
            this.RFD900InfoButton.Text = "Auto Status";
            this.RFD900InfoButton.UseVisualStyleBackColor = true;
            this.RFD900InfoButton.Click += new System.EventHandler(this.RFD900InfoButton_Click);
            // 
            // CRcheckBox
            // 
            this.CRcheckBox.AutoSize = true;
            this.CRcheckBox.Location = new System.Drawing.Point(233, 113);
            this.CRcheckBox.Name = "CRcheckBox";
            this.CRcheckBox.Size = new System.Drawing.Size(63, 17);
            this.CRcheckBox.TabIndex = 98;
            this.CRcheckBox.Text = "Add CR";
            this.CRcheckBox.UseVisualStyleBackColor = true;
            this.CRcheckBox.CheckedChanged += new System.EventHandler(this.CRcheckBox_CheckedChanged);
            // 
            // SendRawButton
            // 
            this.SendRawButton.Location = new System.Drawing.Point(302, 108);
            this.SendRawButton.Name = "SendRawButton";
            this.SendRawButton.Size = new System.Drawing.Size(75, 23);
            this.SendRawButton.TabIndex = 97;
            this.SendRawButton.Text = "Send Raw";
            this.SendRawButton.UseVisualStyleBackColor = true;
            this.SendRawButton.Click += new System.EventHandler(this.SendRawButton_Click);
            // 
            // EnableFileLoggingCheckBox
            // 
            this.EnableFileLoggingCheckBox.AutoSize = true;
            this.EnableFileLoggingCheckBox.Location = new System.Drawing.Point(66, 109);
            this.EnableFileLoggingCheckBox.Name = "EnableFileLoggingCheckBox";
            this.EnableFileLoggingCheckBox.Size = new System.Drawing.Size(131, 17);
            this.EnableFileLoggingCheckBox.TabIndex = 171;
            this.EnableFileLoggingCheckBox.Text = "Enable Logging to File";
            this.EnableFileLoggingCheckBox.UseVisualStyleBackColor = true;
            // 
            // RFD900SerialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1134, 716);
            this.Controls.Add(this.RFD900TabControl);
            this.Name = "RFD900SerialForm";
            this.Text = "RFD900 Tester";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.RFD900TabControl.ResumeLayout(false);
            this.TestCommsPage.ResumeLayout(false);
            this.TestCommsPage.PerformLayout();
            this.LogfileLocationGroupBox.ResumeLayout(false);
            this.LogfileLocationGroupBox.PerformLayout();
            this.TripTimeGroupBox.ResumeLayout(false);
            this.TripTimeGroupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.RFD900SettingsPage.ResumeLayout(false);
            this.RFD900SettingsPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Timer ResponseTimer;
        private System.Windows.Forms.TabControl RFD900TabControl;
        private System.Windows.Forms.TabPage TestCommsPage;
        private System.Windows.Forms.Label ReceivedLabel;
        private System.Windows.Forms.Label TimeoutLabel;
        private System.Windows.Forms.TextBox TimeoutTextBox;
        private System.Windows.Forms.Label IntervalLabel;
        private System.Windows.Forms.TextBox IntervalTextBox;
        private System.Windows.Forms.CheckBox BoxesSilentCheckBox;
        private System.Windows.Forms.Label ClearBoxesLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.ComboBox BaudrateComboBox;
        private System.Windows.Forms.ComboBox COMportComboBox;
        private System.Windows.Forms.Label ReceiveInformationLabel;
        private System.Windows.Forms.RichTextBox ReceiveRichTextBox;
        private System.Windows.Forms.RichTextBox ReceiveInformationRichTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox RunDurationTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox AvgTimeTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox MaxTimeTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox MinTimeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LastTimeTextBox;
        private System.Windows.Forms.CheckBox StopOnErrorCheckBox;
        private System.Windows.Forms.CheckBox RespondEnableCheckBox;
        private System.Windows.Forms.CheckBox SendContinuousCheckBox;
        private System.Windows.Forms.Label ErrorTextLabel;
        private System.Windows.Forms.Label ErrorNumberLabel;
        private System.Windows.Forms.Label SequenceNumberLabel;
        private System.Windows.Forms.Label SequenceStartLabel;
        private System.Windows.Forms.TextBox SequenceStartNumberTextBox;
        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxMessageTextBox;
        private System.Windows.Forms.Label TransmitInformationLabel;
        private System.Windows.Forms.RichTextBox TransmitInformationRichTextBox;
        private System.Windows.Forms.Label TransmittedLabel;
        private System.Windows.Forms.RichTextBox TransmitRichTextBox;
        private System.Windows.Forms.TabPage RFD900SettingsPage;
        private System.Windows.Forms.Label ClearBoxesRFD900Label;
        private System.Windows.Forms.Label ReceivedRFD900Label;
        private System.Windows.Forms.Label ReceiveInformationRFD900Label;
        private System.Windows.Forms.RichTextBox ReceiveRFD900RichTextBox;
        private System.Windows.Forms.RichTextBox ReceiveInformationRFD900RichTextBox;
        private System.Windows.Forms.Label TransmitInformationRFD900Label;
        private System.Windows.Forms.RichTextBox TransmitInformationRFD900RichTextBox;
        private System.Windows.Forms.Label TransmittedRFD900Label;
        private System.Windows.Forms.RichTextBox TransmitRFD900RichTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button RFD900InfoButton;
        private System.Windows.Forms.CheckBox CRcheckBox;
        private System.Windows.Forms.Button SendRawButton;
        private System.Windows.Forms.ComboBox RFD900CommandComboBox;
        private System.Windows.Forms.Button EnterATmodeButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox TripTimeGroupBox;
        private System.Windows.Forms.TextBox FileNameTextBox;
        private System.Windows.Forms.Button BrowseFileNameButton;
        private System.Windows.Forms.CheckBox PromptFileBrowseCheckBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox FileDirectoryTextBox;
        private System.Windows.Forms.GroupBox LogfileLocationGroupBox;
        private System.Windows.Forms.Button FolderExplorerButton;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button ClearOP_RESEND_Button;
        private System.Windows.Forms.Button SetOP_RESEND_Button;
        private System.Windows.Forms.CheckBox EnableFileLoggingCheckBox;
    }
}

