﻿using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;


namespace RFD900TesterApplication
{
    //GUI Generated Application Start
    public partial class RFD900SerialForm : Form
    {
        //RFD900 Specific, Global Variables
        string RxString;
        string NextTxRawString;
        string NextTxMessageString;
        string NextTxResponseString;
        string RxInfoString;
        string TimeoutString;

        Stopwatch my_stopwatch = new Stopwatch();
        Stopwatch runtime_stopwatch = new Stopwatch();
        double minTime, maxTime, totalTime, avgTime, currentTime;
        //TimeSpan RunDuration;
        int totalTrips;

        byte[] uart_rx_buff;
        byte uart_rx_buff_getptr;
        byte uart_rx_buff_putptr;
        byte[] MsgLengthRxBuff;

        int iRxUart_State;

        const int RXUART_STATE_IDLE                 = 0;
        const int RXUART_STATE_WAIT_PREAMBLE_PART2  = 1;
        const int RXUART_STATE_WAIT_COMMAND         = 2;
        const int RXUART_STATE_GET_LENGTH1          = 3;
        const int RXUART_STATE_PROCESS_COMMAND      = 4;

        char ucHostCommand;
        byte ucHostCommandLength;
        byte ucMsgLengthBytesIndex;
        int iPayloadLength;

        byte ucHostCommandIndex;
        const char MESSAGE_FROM_PEER        = 'M';
        const char RESPONSE_FROM_PEER       = 'R';

        int iTransmittedSequenceNumber;
        int iExpectedSequenceNumber;
        int iReceivedSequenceNumber;
        int iSequenceErrorNumber;
        int iTransmitInterval;
        int iTimeoutDuration;

        byte[] RxPayload_buff;
        byte RxPayloadIndex;
        
        bool bAddCarriageReturn;
        bool bSendContinuous;
        bool bTextBoxesSilent;
        bool bResponseEnabled;
        bool bStopOnError;
        bool bFreezeOutputs;

        bool bMessageTransmitBusy;
        bool bResponseTransmitBusy;
        bool bTestingInProgress;

        public RFD900SerialForm()
        {
            InitializeComponent();
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
            {
                //serialPort1.PortName = "COM9";
                serialPort1.PortName = COMportComboBox.Text;
                //serialPort1.BaudRate = 57600;
                serialPort1.BaudRate = Convert.ToInt32(BaudrateComboBox.Text);
                serialPort1.Open();
                TransmitInformationRichTextBox.Text += "Port " + serialPort1.PortName + " Opened @ " + serialPort1.BaudRate + " baud" + System.Environment.NewLine;
                OpenButton.BackColor = System.Drawing.Color.Transparent; //default
                OpenButton.Enabled = false;
                CloseButton.Enabled = true;
                SendButton.Enabled = true;
                if (bSendContinuous)
                    SendButton.BackColor = System.Drawing.Color.LightGreen;
                else
                    SendButton.BackColor = System.Drawing.Color.Transparent;

                RFD900TabControl.Controls.Add(RFD900SettingsPage); //Enable tab
                ReceiveInformationRichTextBox.ReadOnly = false;
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
                TransmitInformationRichTextBox.Text += "Port " + serialPort1.PortName + " Closed" + System.Environment.NewLine;
                OpenButton.Enabled = true;
                OpenButton.BackColor = System.Drawing.Color.LightSalmon;
                CloseButton.Enabled = false;
                SendButton.Enabled = false;
                SendButton.BackColor = System.Drawing.Color.Transparent;
                RFD900TabControl.Controls.Remove(RFD900SettingsPage); //Blank out tab until port is open
                ReceiveInformationRichTextBox.ReadOnly = true;
            }

        }

        private async void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bMessageTransmitBusy = false;
            bResponseTransmitBusy = false;
            bSendContinuous = false;
            bTestingInProgress = false;
            my_stopwatch.Reset();
            runtime_stopwatch.Reset();
            ResponseTimer.Enabled = false;    // Instead of true
            await Task.Delay(iTransmitInterval); //Sleep for a while
            if (serialPort1.IsOpen) serialPort1.Close();
        }

        private void SendRawButton_Click(object sender, EventArgs e)
        {
            string MessageText;
            string SendText;
            if (serialPort1.IsOpen)
            {
                MessageText = RFD900CommandComboBox.Text;
                if (bAddCarriageReturn == true)
                {
                    SendText = MessageText + System.Environment.NewLine;
                }
                else
                {
                    SendText = MessageText;
                }
                serialPort1.Write(SendText);
                TransmitRFD900RichTextBox.Text += SendText + System.Environment.NewLine;
                TransmitInformationRFD900RichTextBox.Text += "Sending Raw Text from box" + System.Environment.NewLine;
            }
        }

        string TrimFromZero(string input)
        {
            int index = input.IndexOf('\0');
            if (index < 0)
                return input;

            return input.Substring(0, index);
        }


        private async void SendNewResponse(object sender, EventArgs e)
        {
            string MessageText;
            int iMessageLength;
            string SendText;
            string DecLength;

            while (bMessageTransmitBusy)
            {
                await Task.Delay(1); //Sleep for a while
            }
            bResponseTransmitBusy = true;
            if (serialPort1.IsOpen)
            {
                MessageText = TrimFromZero(NextTxResponseString);
                iMessageLength = MessageText.Length;
                DecLength = iMessageLength.ToString("D3");
                SendText = "=>R" + DecLength + MessageText;
                serialPort1.Write(SendText);
                if (!bTextBoxesSilent && !bFreezeOutputs)
                {
                    TransmitRichTextBox.Text += SendText + System.Environment.NewLine;
                    TransmitInformationRichTextBox.Text += "Sending response to message" + System.Environment.NewLine;
                }
            }
            bResponseTransmitBusy = false;
        }


        public static Task Delay(int milliseconds)
        {
            var tcs = new TaskCompletionSource<bool>();
            var timer = new System.Threading.Timer(o => tcs.SetResult(false));
            timer.Change(milliseconds, -1);
            return tcs.Task;
        }


        private async void ClickSendButtonProgrammatically(object sender, EventArgs e)
        {
            string MessageText;
            int iMessageLength;
            string SendText;
            string DecLength;
            string SequenceText;

            if (bTestingInProgress == false)
            {
                return; //do nothing here
            }
            await Task.Delay(iTransmitInterval); //Sleep for a while
            while (bResponseTransmitBusy)
            {
                await Task.Delay(1); //Sleep for a while
            }
            bMessageTransmitBusy = true;
            if (serialPort1.IsOpen)
            {
                iTransmittedSequenceNumber++;
                SequenceNumberLabel.Text = iTransmittedSequenceNumber.ToString("D6");
                iExpectedSequenceNumber = iTransmittedSequenceNumber;
                SequenceText = iTransmittedSequenceNumber.ToString("D6");
                //MessageText = SequenceText + TrimFromZero(NextTxString);
                //MessageText = SequenceText + TxMessageTextBox.Text;
                MessageText = SequenceText + TxMessageTextBox.Text + " Time:" + DateTime.Now;
                RunDurationTextBox.Text = runtime_stopwatch.Elapsed.ToString(@"dd\.hh\:mm\:ss");
                my_stopwatch.Reset();
                my_stopwatch.Start();
                iMessageLength = MessageText.Length;
                DecLength = iMessageLength.ToString("D3");
                SendText = "=>M" + DecLength + MessageText;
                serialPort1.Write(SendText);
                RestartTimer();
                if (!bTextBoxesSilent && !bFreezeOutputs)
                {
                    TransmitRichTextBox.Text += SendText + System.Environment.NewLine;
                    TransmitInformationRichTextBox.Text += "Sending text with length " + DecLength + " and sequence# " + SequenceText + System.Environment.NewLine;
                }

            }
            bMessageTransmitBusy = false;
        }

        private async void SendButton_Click(object sender, EventArgs e)
        {
            string MessageText;
            int iMessageLength;
            string SendText;
            string DecLength;
            string SequenceText;

            bFreezeOutputs = false;
            if (bSendContinuous)
            {
                bTestingInProgress = !bTestingInProgress;
                if (bTestingInProgress)
                {
                    SendButton.BackColor = System.Drawing.Color.LightSalmon;
                    SendButton.Text = "Stop";
                    if (PromptFileBrowseCheckBox.Checked && EnableFileLoggingCheckBox.Checked)
                    { //Prompt for a new file name to use
                        BrowseFileName();
                    }
                    if (EnableFileLoggingCheckBox.Checked)
                    { //Prompt for a new file name to use
                        this.Invoke(new EventHandler(ComposeLogFileHeader));
                    }

                }
                else
                {
                    SendButton.BackColor = System.Drawing.Color.LightGreen;
                    SendButton.Text = "Start";
                    return; //do nothing here
                }
            }
                
                while (bResponseTransmitBusy)
            {
                await Task.Delay(1); //Sleep for a while
            }
            bMessageTransmitBusy = true;
            if (serialPort1.IsOpen)
            {
                iSequenceErrorNumber = 0;
                this.Invoke(new EventHandler(DisplayErrorNumber));
                iTransmittedSequenceNumber = Convert.ToInt32(SequenceStartNumberTextBox.Text);
                SequenceNumberLabel.Text = iTransmittedSequenceNumber.ToString("D6");
                iExpectedSequenceNumber = iTransmittedSequenceNumber;
                SequenceText = iTransmittedSequenceNumber.ToString("D6");
                //MessageText = SequenceText + TxMessageTextBox.Text;
                MessageText = SequenceText + TxMessageTextBox.Text + " Time:" + DateTime.Now;
                minTime = 10000000; 
                maxTime=0; 
                totalTime=0;
                avgTime = 0; 
                currentTime=0;
                totalTrips=0;
                runtime_stopwatch.Reset();
                runtime_stopwatch.Restart();
                //RunDuration = TimeSpan.Zero;
                RunDurationTextBox.Text = runtime_stopwatch.Elapsed.ToString(@"dd\.hh\:mm\:ss");
                my_stopwatch.Reset();
                my_stopwatch.Start();
                iMessageLength = MessageText.Length;
                DecLength = iMessageLength.ToString("D3");
                SendText = "=>M" + DecLength + MessageText;
                serialPort1.Write(SendText);
                RestartTimer();
                if (!bTextBoxesSilent && !bFreezeOutputs)
                {
                    TransmitRichTextBox.Text += SendText + System.Environment.NewLine;
                    TransmitInformationRichTextBox.Text += "Sending text with length " + DecLength + " and sequence# " + SequenceText + System.Environment.NewLine;
                }

            }
            bMessageTransmitBusy = false;
        }

        private void SendNewRawMessage(object sender, EventArgs e)
        {
            string MessageText;
            int iMessageLength;
            string SendText;

            if (serialPort1.IsOpen)
            {
                MessageText = TrimFromZero(NextTxRawString);
                iMessageLength = MessageText.Length;
                SendText = MessageText;
                serialPort1.Write(SendText);
                TransmitRFD900RichTextBox.Text += SendText + System.Environment.NewLine;
                TransmitInformationRFD900RichTextBox.Text += "Sending Raw message" + System.Environment.NewLine;
            }
        }

        private void DisplayText(object sender, EventArgs e)
        {
            if (!bTextBoxesSilent && !bFreezeOutputs)
            {
                ReceiveRichTextBox.AppendText(RxString);
                ReceiveRFD900RichTextBox.AppendText(RxString);
            }
        }

        private void DisplayRxInfoText(object sender, EventArgs e)
        {
            if (!bTextBoxesSilent && !bFreezeOutputs)
            {
                ReceiveInformationRichTextBox.Text += RxInfoString;
                ReceiveInformationRichTextBox.Text += System.Environment.NewLine;
            }
        }

        private void DisplayElapsedTime(object sender, EventArgs e)
        {
            currentTime = my_stopwatch.Elapsed.TotalMilliseconds;
            totalTime += currentTime;
            totalTrips++;
            avgTime = totalTime / totalTrips;

            if (currentTime < minTime)
            {
                minTime = currentTime;
            }

            if (currentTime > maxTime)
            {
                maxTime = currentTime;
            }
            //LastTimeTextBox.Text = currentTime.ToString();
            //MinTimeTextBox.Text = minTime.ToString();
            //MaxTimeTextBox.Text = maxTime.ToString();
            //AvgTimeTextBox.Text = avgTime.ToString();
            LastTimeTextBox.Text = string.Format("{0:0.00000}", currentTime); //Fix to 5 decimal places
            MinTimeTextBox.Text = string.Format("{0:0.00000}", minTime); //Fix to 5 decimal places
            MaxTimeTextBox.Text = string.Format("{0:0.00000}", maxTime); //Fix to 5 decimal places
            AvgTimeTextBox.Text = string.Format("{0:0.00000}", avgTime); //Fix to 5 decimal places
            RunDurationTextBox.Text = runtime_stopwatch.Elapsed.ToString(@"dd\.hh\:mm\:ss");
        }

        private void ComposeLogFileHeader(object sender, EventArgs e)
        {
            //Write strings to the log file using:
            //WriteToMyTextfile(somestring) or 
            //WriteLineToMyTextfile(somestring)
            string StringForMyFile;
            StringForMyFile = "";
            WriteLineToMyTextfile(StringForMyFile);
            StringForMyFile = "RFD900 Log file " + FileNameTextBox.Text + " created on " + DateTime.Now;
            WriteLineToMyTextfile(StringForMyFile);
            StringForMyFile = "Last,Min,Max,Avg,TotalTime,RunTimeDHMS,Sequence#,Errors,DateTime";
            WriteLineToMyTextfile(StringForMyFile);

        }

        private void UpdateLogFile(object sender, EventArgs e)
        {
            //Write strings to the log file using:
            //WriteToMyTextfile(somestring) or    no line feed
            //WriteLineToMyTextfile(somestring)   makes a new line
            //When a .csv file is used, separate fields with commas
            string StringForMyFile;
            StringForMyFile = string.Format("{0:0.00000}", currentTime); //Fix to 5 decimal places
            StringForMyFile += "," + string.Format("{0:0.00000}", minTime); //Fix to 5 decimal places
            StringForMyFile += "," + string.Format("{0:0.00000}", maxTime); //Fix to 5 decimal places
            StringForMyFile += "," + string.Format("{0:0.00000}", avgTime); //Fix to 5 decimal places
            StringForMyFile += "," + string.Format("{0:0.00000}", totalTime); //Fix to 5 decimal places
            StringForMyFile += "," + RunDurationTextBox.Text;
            StringForMyFile += "," + iReceivedSequenceNumber.ToString("D6");
            StringForMyFile += "," + iSequenceErrorNumber.ToString("D6");
            StringForMyFile += "," + DateTime.Now;
            WriteLineToMyTextfile(StringForMyFile);

        }


        private void DisplayErrorNumber(object sender, EventArgs e)
        {
            ErrorNumberLabel.Visible = true;
            ErrorTextLabel.Visible = true;
            ErrorNumberLabel.Text = iSequenceErrorNumber.ToString("D");
            if (iSequenceErrorNumber > 0)
            {
                ErrorNumberLabel.ForeColor = System.Drawing.Color.Red;
                ErrorTextLabel.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                ErrorNumberLabel.ForeColor = System.Drawing.Color.Green;
                ErrorTextLabel.ForeColor = System.Drawing.Color.Green;
            }
            if (bStopOnError && (iSequenceErrorNumber > 0))
            {
                bTestingInProgress = false;
                if (bSendContinuous)
                {
                    SendButton.BackColor = System.Drawing.Color.LightGreen;
                    SendButton.Text = "Start";
                }
                else
                {
                    SendButton.BackColor = System.Drawing.Color.Transparent;
                    SendButton.Text = "Send";
                }
                //BoxesSilentCheckBox.Checked = true;
                //bTextBoxesSilent = true;
                bFreezeOutputs = true;
            }

        }

        private void DisplayTimeoutString(object sender, EventArgs e)
        {
                TransmitInformationRichTextBox.Text += TimeoutString;
        }

        void DecodeUartRx()
        {
            if (RxPayload_buff == null)
            {
                RxPayload_buff = new byte[256]; //initialize array
                RxPayloadIndex = 0;
            }
            if (MsgLengthRxBuff == null)
            {
                MsgLengthRxBuff = new byte[3]; //initialize array
            }
            while (uart_rx_buff_getptr != uart_rx_buff_putptr)
            {
                switch (iRxUart_State)
                {
                    case RXUART_STATE_IDLE:
                        {
                            // Look for '=' :
                            if ('=' == (char)uart_rx_buff[uart_rx_buff_getptr++])
                            {
                                iRxUart_State = RXUART_STATE_WAIT_PREAMBLE_PART2;
                            }
                            break;
                        }
                    case RXUART_STATE_WAIT_PREAMBLE_PART2:
                        {
                            // Look for '=' :
                            if ('>' == (char)uart_rx_buff[uart_rx_buff_getptr++])
                            {
                                ucMsgLengthBytesIndex = 0;
                                iRxUart_State = RXUART_STATE_WAIT_COMMAND;
                            }
                            else
                            {
                                iRxUart_State = RXUART_STATE_IDLE;
                            }
                            break;
                        }
                    case RXUART_STATE_WAIT_COMMAND:
                        {
                            // Look to see if a new command has been received:
                            ucHostCommand = (char)uart_rx_buff[uart_rx_buff_getptr++];
                            ucHostCommandLength = 0;
                            iRxUart_State = RXUART_STATE_GET_LENGTH1;
                            break;
                        }
                    case RXUART_STATE_GET_LENGTH1:
                        {
                            MsgLengthRxBuff[ucMsgLengthBytesIndex++] = Convert.ToByte(uart_rx_buff[uart_rx_buff_getptr++] - Convert.ToByte(0x30)); // The bytes are in ASCII
                            if (ucMsgLengthBytesIndex ==3)
                            { // all three length bytes received
                                iPayloadLength = MsgLengthRxBuff[2] + 10*MsgLengthRxBuff[1] + 100*MsgLengthRxBuff[0];
                                iRxUart_State = RXUART_STATE_PROCESS_COMMAND;
                                RxPayloadIndex = 0;
                            }
                            break;
                        }
                    case RXUART_STATE_PROCESS_COMMAND:
                        {
                            switch (ucHostCommand)
                            {
                                case MESSAGE_FROM_PEER:
                                    {
                                        //Receive bytes (text):
                                        RxPayload_buff[RxPayloadIndex++] = uart_rx_buff[uart_rx_buff_getptr++];
                                        iPayloadLength--;
                                        if (iPayloadLength == 0)
                                        {
                                            iRxUart_State = RXUART_STATE_IDLE;
                                            RxPayload_buff[RxPayloadIndex++] = 0; // Null terminate the received string
                                            string RxString = TrimFromZero(System.Text.ASCIIEncoding.ASCII.GetString(RxPayload_buff));
                                            string ReceivedSequenceText = RxString.Substring(0, 6);
                                            RxInfoString = "Received Message with sequence " + ReceivedSequenceText + " : " + RxString.Substring(6);
                                            this.Invoke(new EventHandler(DisplayRxInfoText));
                                            this.Invoke(new EventHandler(DisplayErrorNumber));
                                            NextTxResponseString = RxString;
                                            if (bResponseEnabled)
                                            {
                                                this.Invoke(new EventHandler(SendNewResponse));
                                            }
                                        }
                                        break;
                                    }
                                case RESPONSE_FROM_PEER:
                                    {
                                        //Receive bytes (text):
                                        RxPayload_buff[RxPayloadIndex++] = uart_rx_buff[uart_rx_buff_getptr++];
                                        iPayloadLength--;
                                        if (iPayloadLength == 0)
                                        {
                                            iRxUart_State = RXUART_STATE_IDLE;
                                            RxPayload_buff[RxPayloadIndex++] = 0; // Null terminate the received string
                                            string RxString = TrimFromZero(System.Text.ASCIIEncoding.ASCII.GetString(RxPayload_buff));
                                            string ReceivedSequenceText = RxString.Substring(0, 6);

                                            int iTryReceivedSequenceNumber;
                                            if (Int32.TryParse(ReceivedSequenceText, out iTryReceivedSequenceNumber))
                                            {
                                                iReceivedSequenceNumber = iTryReceivedSequenceNumber;
                                                if (iReceivedSequenceNumber != iExpectedSequenceNumber)
                                                {
                                                    iSequenceErrorNumber++;
                                                }

                                            }
                                            else
                                            {  //the number itself is messed up
                                                iSequenceErrorNumber++;
                                            }
                                            RxInfoString = "Received Response with sequence " + ReceivedSequenceText + " : " + RxString.Substring(6);
                                            this.Invoke(new EventHandler(DisplayRxInfoText));
                                            this.Invoke(new EventHandler(DisplayErrorNumber));
                                            //We have received a response, now process some statistics and logs:
                                            my_stopwatch.Stop();
                                            this.Invoke(new EventHandler(DisplayElapsedTime));
                                            if (EnableFileLoggingCheckBox.Checked)
                                                this.Invoke(new EventHandler(UpdateLogFile));
                                            //See if we need to automatically send out the next message:
                                            if (bSendContinuous)
                                            {
                                                NextTxMessageString = RxString.Substring(6);
                                                this.Invoke(new EventHandler(ClickSendButtonProgrammatically));
                                            }
                                        }
                                        break;

                                    }
                                default:  /* Unknown Command */
                                    {
                                        iRxUart_State = RXUART_STATE_IDLE;
                                        break;
                                    }
                            } //switch
                            break;
                        }

                    default:  /* Unknown state */
                        {
                            iRxUart_State = RXUART_STATE_WAIT_COMMAND;
                            break;
                        }
                } //switch
            }  //while
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (uart_rx_buff == null)
            {
                uart_rx_buff = new byte[256]; //initialize array
                uart_rx_buff_getptr = 0;
                uart_rx_buff_putptr = 0;
            }
            //Initialize a buffer to hold the received data 
            byte[] buffer = new byte[serialPort1.ReadBufferSize + 1];
            //There is no accurate method for checking how many bytes are read 
            //unless you check the return from the Read method 
            int bytesRead = serialPort1.Read(buffer, 0, buffer.Length);
            int i;
            for (i = 0; i < bytesRead; i++)
            {
                uart_rx_buff[uart_rx_buff_putptr++] = buffer[i];
            }

            buffer[serialPort1.ReadBufferSize] = 0; //Null terminate the string in case
            RxString = System.Text.Encoding.UTF8.GetString(buffer);
            this.Invoke(new EventHandler(DisplayText));
            DecodeUartRx();

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

            OpenButton.Enabled = true;
            OpenButton.BackColor = System.Drawing.Color.LightSalmon;

            iRxUart_State = RXUART_STATE_IDLE;
            ucHostCommand  = 'Z'; //unknown
            iPayloadLength = 0;

            CloseButton.Enabled = false;
            SendButton.Enabled = false;
            ReceiveRichTextBox.ReadOnly = true;
            TransmitRichTextBox.ReadOnly = true;
            ReceiveInformationRichTextBox.ReadOnly = true;
            ErrorNumberLabel.Visible = false;
            ErrorTextLabel.Visible = false;
            TestCommsPage.BackColor = default(Color);
            RFD900SettingsPage.BackColor = default(Color);
            RFD900TabControl.Controls.Remove(RFD900SettingsPage); //Blank out tab until port is open
            ReceiveInformationRFD900Label.Visible = false;
            ReceiveInformationRFD900RichTextBox.Visible = false;


            iTransmitInterval = Convert.ToInt32(IntervalTextBox.Text);
            bTestingInProgress = false;

            CRcheckBox.Checked = true;
            bAddCarriageReturn = true;
            SendContinuousCheckBox.Checked = true;
            bSendContinuous = true;
            SendButton.Text = "Start";
            //SendButton.BackColor = System.Drawing.Color.LightGreen;

            StopOnErrorCheckBox.Checked = true;
            bStopOnError = true;
            bFreezeOutputs = false;

            bTextBoxesSilent = false;
            RespondEnableCheckBox.Checked = true;
            bResponseEnabled = true;

            //Initialize the file name and directory textboxes:
            string OldDirectoryPath = Directory.GetCurrentDirectory(); //This is where the executable is running
            string MyTargetDirectory = OldDirectoryPath + @"\logs"; //make a "logs" directory in the executable folder
            if (!Directory.Exists(MyTargetDirectory))
            {
                Directory.CreateDirectory(MyTargetDirectory);
            }
            FileDirectoryTextBox.Text = MyTargetDirectory;
            //FileNameTextBox.Text = "CurrentTestLocation"; //default file name (.txt is added when we write to it)
            FileNameTextBox.Text = "CurrentTestLocation.csv"; //default file name
            PromptFileBrowseCheckBox.Checked = true; //this might become irritating!
            EnableFileLoggingCheckBox.Checked = true;

            BaudrateComboBox.Items.Add("57600");
            BaudrateComboBox.Items.Add("115200");
            //BaudrateComboBox.SelectedIndex = 0; //first item as default
            BaudrateComboBox.SelectedIndex = 1; //second item as default

            // Get a list of serial port names.
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            Console.WriteLine("The following serial ports were found:");

            int NumPortsFound = 0;
            // Display each port name to the console. 
            foreach (string port in ports)
            {
                NumPortsFound++;
                Console.WriteLine(port);
                COMportComboBox.Items.Add(port);
            }
            string PreferredPort = "COM13";
            //string PreferredPort = "COM16";
            if (NumPortsFound > 0)
            {
                if (COMportComboBox.FindString(PreferredPort) >= 0)
                {
                    COMportComboBox.SelectedIndex = COMportComboBox.FindString(PreferredPort);
                }
                else
                {
                    COMportComboBox.SelectedIndex = 0; //first one found
                }

            }
            else
            { //No ports available
                COMportComboBox.BackColor = System.Drawing.Color.LightSalmon;
                OpenButton.Enabled = false;
            }

        }

        private void TransmitInformationLabel_Click(object sender, EventArgs e)
        {
            TransmitInformationRichTextBox.Text = ""; // Clear Box
        }

        private void ReceiveInformationLabel_Click(object sender, EventArgs e)
        {
            ReceiveInformationRichTextBox.Text = ""; // Clear Box
        }

        private void ReceivedLabel_Click(object sender, EventArgs e)
        {
            ReceiveRichTextBox.Text = ""; // Clear Box
        }

        private void TransmittedLabel_Click(object sender, EventArgs e)
        {
            TransmitRichTextBox.Text = ""; // Clear Box
        }

        private void ClearBoxesLabel_Click(object sender, EventArgs e)
        {
            ReceiveInformationRichTextBox.Text = ""; // Clear Box
            ReceiveRichTextBox.Text = ""; // Clear Box
            TransmitInformationRichTextBox.Text = ""; // Clear Box
            TransmitRichTextBox.Text = ""; // Clear Box
        }

        private void TransmitInformationRichTextBox_TextChanged(object sender, EventArgs e)
        {
            TransmitInformationRichTextBox.SelectionStart = TransmitInformationRichTextBox.Text.Length; //Set the current caret position at the end
            TransmitInformationRichTextBox.ScrollToCaret(); //Now scroll it automatically

            //When the box gets very full, delete the oldest data
            //if (TransmitInformationRichTextBox.Text.Length > 10000)
            //{
            //    TransmitInformationRichTextBox.Select(0, 2000);
            //    TransmitInformationRichTextBox.SelectedText = "";
            //}
        }

        private void ReceiveInformationRichTextBox_TextChanged(object sender, EventArgs e)
        {
            ReceiveInformationRichTextBox.SelectionStart = ReceiveInformationRichTextBox.Text.Length; //Set the current caret position at the end
            ReceiveInformationRichTextBox.ScrollToCaret(); //Now scroll it automatically

            //When the box gets very full, delete the oldest data
            //if (ReceiveInformationRichTextBox.Text.Length > 10000)
            //{
            //    ReceiveInformationRichTextBox.Select(0, 2000);
            //    ReceiveInformationRichTextBox.SelectedText = "";
            //}
        }

        private void ReceiveRichTextBox_TextChanged(object sender, EventArgs e)
        {
            ReceiveRichTextBox.SelectionStart = ReceiveRichTextBox.Text.Length; //Set the current caret position at the end
            ReceiveRichTextBox.ScrollToCaret(); //Now scroll it automatically

            //When the box gets very full, delete the oldest data
            //if (ReceiveRichTextBox.Text.Length > 10000)
            //{
            //    ReceiveRichTextBox.Select(0, 2000);
            //    ReceiveRichTextBox.SelectedText = "";
            //}
        }

        private void TransmitRichTextBox_TextChanged(object sender, EventArgs e)
        {
            TransmitRichTextBox.SelectionStart = TransmitRichTextBox.Text.Length; //Set the current caret position at the end
            TransmitRichTextBox.ScrollToCaret(); //Now scroll it automatically

            //When the box gets very full, delete the oldest data
            //if (TransmitRichTextBox.Text.Length > 10000)
            //{
            //    TransmitRichTextBox.Select(0, 2000);
            //    TransmitRichTextBox.SelectedText = "";
            //}
        }

        private void TransmitInformationRFD900Label_Click(object sender, EventArgs e)
        {
            TransmitInformationRFD900RichTextBox.Text = ""; // Clear Box
        }

        private void ReceiveInformationRFD900Label_Click(object sender, EventArgs e)
        {
            ReceiveInformationRFD900RichTextBox.Text = ""; // Clear Box
        }

        private void ReceivedRFD900Label_Click(object sender, EventArgs e)
        {
            ReceiveRFD900RichTextBox.Text = ""; // Clear Box
        }

        private void TransmittedRFD900Label_Click(object sender, EventArgs e)
        {
            TransmitRFD900RichTextBox.Text = ""; // Clear Box
        }

        private void ClearBoxesRFD900Label_Click(object sender, EventArgs e)
        {
            ReceiveInformationRFD900RichTextBox.Text = ""; // Clear Box
            ReceiveRFD900RichTextBox.Text = ""; // Clear Box
            TransmitInformationRFD900RichTextBox.Text = ""; // Clear Box
            TransmitRFD900RichTextBox.Text = ""; // Clear Box
        }

        private void TransmitInformationRFD900RichTextBox_TextChanged(object sender, EventArgs e)
        {
            TransmitInformationRFD900RichTextBox.SelectionStart = TransmitInformationRFD900RichTextBox.Text.Length; //Set the current caret position at the end
            TransmitInformationRFD900RichTextBox.ScrollToCaret(); //Now scroll it automatically
        }

        private void ReceiveInformationRFD900RichTextBox_TextChanged(object sender, EventArgs e)
        {
            ReceiveInformationRFD900RichTextBox.SelectionStart = ReceiveInformationRFD900RichTextBox.Text.Length; //Set the current caret position at the end
            ReceiveInformationRFD900RichTextBox.ScrollToCaret(); //Now scroll it automatically
        }

        private void ReceiveRFD900RichTextBox_TextChanged(object sender, EventArgs e)
        {
            ReceiveRFD900RichTextBox.SelectionStart = ReceiveRFD900RichTextBox.Text.Length; //Set the current caret position at the end
            ReceiveRFD900RichTextBox.ScrollToCaret(); //Now scroll it automatically
        }

        private void TransmitRFD900RichTextBox_TextChanged(object sender, EventArgs e)
        {
            TransmitRFD900RichTextBox.SelectionStart = TransmitRFD900RichTextBox.Text.Length; //Set the current caret position at the end
            TransmitRFD900RichTextBox.ScrollToCaret(); //Now scroll it automatically
        }

        private void CRcheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (CRcheckBox.Checked == true)
            {
                bAddCarriageReturn = true;
            }
            else
            {
                bAddCarriageReturn = false;
            }

        }

        private async void RFD900InfoButton_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                NextTxRawString = "+++"; //Enter AT mode
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(1200); //Sleep for a while
                NextTxRawString = "ATI" + System.Environment.NewLine; //Send AT message to get device info
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(500); //Sleep for a while
                NextTxRawString = "ATI1" + System.Environment.NewLine; //Send AT message to get device info
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(500); //Sleep for a while
                NextTxRawString = "ATI2" + System.Environment.NewLine; //Send AT message to get device info
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(500); //Sleep for a while
                NextTxRawString = "ATI3" + System.Environment.NewLine; //Send AT message to get device info
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(500); //Sleep for a while
                NextTxRawString = "ATI4" + System.Environment.NewLine; //Send AT message to get device info
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(500); //Sleep for a while
                NextTxRawString = "ATI5" + System.Environment.NewLine; //Send AT message to get device info
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(500); //Sleep for a while
                NextTxRawString = "ATO" + System.Environment.NewLine; //Exit AT mode
                this.Invoke(new EventHandler(SendNewRawMessage));
            }

        }

        private async void SetOP_RESEND_Button_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                NextTxRawString = "+++"; //Enter AT mode
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(1200); //Sleep for a while
                NextTxRawString = "ATS7=1" + System.Environment.NewLine; //Send AT message to set bit ATS7 (OP_RESEND) = 1 (default and works with TP-Link)
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(500); //Sleep for a while
                NextTxRawString = "AT&W" + System.Environment.NewLine; //Send AT message to write current parameters to EEPROM
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(1000); //Sleep for a while
                NextTxRawString = "ATO" + System.Environment.NewLine; //Exit AT mode
                this.Invoke(new EventHandler(SendNewRawMessage));
            }

        }

        private async void ClearOP_RESEND_Button_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                NextTxRawString = "+++"; //Enter AT mode
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(1200); //Sleep for a while
                NextTxRawString = "ATS7=0" + System.Environment.NewLine; //Send AT message to set bit ATS7 (OP_RESEND) = 0 (to work with this program because no comms errors)
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(500); //Sleep for a while
                NextTxRawString = "AT&W" + System.Environment.NewLine; //Send AT message to write current parameters to EEPROM
                this.Invoke(new EventHandler(SendNewRawMessage));
                await Task.Delay(1000); //Sleep for a while
                NextTxRawString = "ATO" + System.Environment.NewLine; //Exit AT mode
                this.Invoke(new EventHandler(SendNewRawMessage));
            }

        }


        private void SendContinuousCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (SendContinuousCheckBox.Checked == true)
            {
                bSendContinuous = true;
                SendButton.Text = "Start";
                if (serialPort1.IsOpen)
                    SendButton.BackColor = System.Drawing.Color.LightGreen;
            }
            else
            {
                bSendContinuous = false;
                ResponseTimer.Enabled = false;

                SendButton.Text = "Send";
                SendButton.BackColor = System.Drawing.Color.Transparent;
            }
        }

        private void BoxesSilentCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (BoxesSilentCheckBox.Checked == true)
                bTextBoxesSilent = true;
            else
                bTextBoxesSilent = false;
        }

        private void IntervalTextBox_TextChanged(object sender, EventArgs e)
        {
            Int32.TryParse(IntervalTextBox.Text, out iTransmitInterval);
            //iTransmitInterval = Convert.ToInt32(IntervalTextBox.Text);
        }

        private void ResponseTimer_Tick(object sender, EventArgs e)
        {
            ResponseTimer.Enabled = false;
            if (bTestingInProgress)
            {
                TimeoutString = "Timeout Expired (Rx state " + iRxUart_State + ", cmd " + ucHostCommand + ", payload remaining " + iPayloadLength + ")" + System.Environment.NewLine;
                this.Invoke(new EventHandler(DisplayTimeoutString));
                iSequenceErrorNumber++;
                this.Invoke(new EventHandler(DisplayErrorNumber));
                NextTxMessageString = TxMessageTextBox.Text;  //this will only get used when we do not stop on error
                this.Invoke(new EventHandler(ClickSendButtonProgrammatically));
            }
        }

        private void StartTimer()
        {
            if (Int32.TryParse(TimeoutTextBox.Text, out iTimeoutDuration))
                ResponseTimer.Interval = iTimeoutDuration;
            ResponseTimer.Enabled = true;
            ResponseTimer.Start();
        }

        private void RestartTimer()
        {
            ResponseTimer.Stop();
            if (Int32.TryParse(TimeoutTextBox.Text, out iTimeoutDuration))
                ResponseTimer.Interval = iTimeoutDuration;
            ResponseTimer.Enabled = true;
            ResponseTimer.Start();
        }

        private void RespondEnableCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (RespondEnableCheckBox.Checked == true)
            {
                bResponseEnabled = true;
            }
            else
            {
                bResponseEnabled = false;
            }
        }

        private void ErrorNumberLabel_DoubleClick(object sender, EventArgs e)
        {
            iSequenceErrorNumber = 0; //reset the error number
            this.Invoke(new EventHandler(DisplayErrorNumber));
        }

        private void StopOnErrorCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (StopOnErrorCheckBox.Checked == true)
            {
                bStopOnError = true;
            }
            else
            {
                bStopOnError = false;
            }

        }

        private void EnterATmodeButton_Click(object sender, EventArgs e)
        {
            string MessageText;
            string SendText;
            if (serialPort1.IsOpen)
            {
                MessageText = "+++";
                SendText = MessageText;
                serialPort1.Write(SendText);
                TransmitRFD900RichTextBox.Text += SendText + System.Environment.NewLine;
                TransmitInformationRFD900RichTextBox.Text += "Sending +++ (without Carriage Return) to enter Hayes AT mode" + System.Environment.NewLine;
            }

        }

        private void WriteFileButton_Click(object sender, EventArgs e)
        {
            //WriteToMyTextfile(DateTime.Now.ToString());
            WriteLineToMyTextfile(DateTime.Now.ToString());
        }


        private void WriteToMyTextfile(string MyStringToWrite)
        {
            //string myfilename = FileDirectoryTextBox.Text + @"\" + FileNameTextBox.Text + ".txt";
            string myfilename = FileDirectoryTextBox.Text + @"\" + FileNameTextBox.Text;
            using (StreamWriter myfile = new StreamWriter(myfilename, true)) //append if it already exists
            {
                myfile.Write(MyStringToWrite);
            }
        }

        private void WriteLineToMyTextfile(string MyStringToWrite)
        {
            //string myfilename = FileDirectoryTextBox.Text + @"\" + FileNameTextBox.Text + ".txt";
            string myfilename = FileDirectoryTextBox.Text + @"\" + FileNameTextBox.Text;
            using (StreamWriter myfile = new StreamWriter(myfilename, true)) //append if it already exists
            {
                myfile.WriteLine(MyStringToWrite);
            }
        }

        private void BrowseFileName()
        {
            // Displays a SaveFileDialog so the user can pick or create a file
            using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
            {
                saveFileDialog1.InitialDirectory = FileDirectoryTextBox.Text;
                saveFileDialog1.Filter = "Commma Delimited Text (.csv)|*.csv|Text File (.txt)|*.txt|All files (*.*)|*.*";
                saveFileDialog1.Title = "Pick an RFD900 Log File to Save to";
                saveFileDialog1.FileName = "NewTestLocation"; //Some default file name
                saveFileDialog1.OverwritePrompt = false; //do not warn of overwrite
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (saveFileDialog1.FileName != "")
                    {
                        string my_file_name = saveFileDialog1.FileName;
                        //FileNameTextBox.Text = Path.GetFileNameWithoutExtension(my_file_name);
                        FileNameTextBox.Text = Path.GetFileName(my_file_name);
                        FileDirectoryTextBox.Text = Path.GetDirectoryName(my_file_name);
                    }
                }

            }
        }
        
        
        private void BrowseFileNameButton_Click(object sender, EventArgs e)
        {
            BrowseFileName();
        }

        private void FolderExplorerButton_Click(object sender, EventArgs e)
        {
            Process.Start(FileDirectoryTextBox.Text);
        }





    }               
}
